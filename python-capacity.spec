%global _empty_manifest_terminate_build 0
Name:           python-capacity
Version:        1.3.14
Release:        1
Summary:        Data types to describe capacity
License:        BSD
URL:            https://pypi.org/project/capacity/
Source0:        https://files.pythonhosted.org/packages/f1/e6/46e7923be76c97475daf23138a8d751ddb7507409d341afb55cc8130e76c/capacity-1.3.14.tar.gz
BuildArch:      noarch
%description
Data types to describe capacity

%package -n python3-capacity
Summary:        Data types to describe capacity
Provides:       python-capacity
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
%description -n python3-capacity
Data types to describe capacity

%package help
Summary:        Data types to describe capacity
Provides:       python3-capacity-doc
%description help
Data types to describe capacity

%prep
%autosetup -n capacity-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-capacity -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Aug 05 2021 OpenStack_SIG <openstack@openeuler.org> - 1.3.14-1
- Package Spec generate
